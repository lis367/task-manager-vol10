package com.company.Interfaces;

import com.company.entity.Project;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface ProjectServiceInterface {


    String projectCreate(String name, Date dateStart, Date dateEnd, String userId, String description);
    Project read(String id) throws ObjectIsNotFound;
    void update(Project project);
    void projectRemove(String id) throws ObjectIsNotFound;
    ArrayList projectList();
    void projectClear();
    void setProjectRepository(ProjectRepoInterface projectRepository);
    void setTaskRepository(TaskRepoInterface taskRepository);
    void setBootstrap (Bootstrap bootstrap);
    void setProjects(List<Project> projects) throws EmptyField;
    List allProjectList();
    void load() throws IOException, ClassNotFoundException;
    void saveInDb();






}
