package com.company.commands;

import com.company.Interfaces.ServiceLocator;
import lombok.NoArgsConstructor;


import java.io.InputStream;
import java.util.Properties;
@NoArgsConstructor


public class AboutCommand extends AbstractCommand {

    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Showing current version";
    }

    @Override
    public void execute() throws Exception {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try (InputStream is = loader.getResourceAsStream("application.properties")) {
            properties.load(is);
        }
        String version = properties.getProperty("application.version");
        System.out.println("Current version is " +version);

    }

    @Override
    public boolean secureCommand() {
        return false;
    }

    public AboutCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

}
