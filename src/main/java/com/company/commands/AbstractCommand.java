package com.company.commands;


import com.company.Interfaces.ServiceLocator;

public abstract class AbstractCommand {
    public abstract String command();
    public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean secureCommand();
    public ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


}

