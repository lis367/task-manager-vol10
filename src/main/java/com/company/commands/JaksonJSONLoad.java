package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JaksonJSONLoad extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonjson-load";
    }

    @Override
    public String description() {
        return "jaksonjson-load";
    }

    @Override
    public void execute() throws Exception {
        final String jaksonJSON = "src/main/resources/Jaxb/JAKSONJson.json";
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        ObjectMapper mapper = new ObjectMapper();
        try {
            Data data = mapper.readValue(new File(jaksonJSON),Data.class);
            dataService.load(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
