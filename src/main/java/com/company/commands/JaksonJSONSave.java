package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;

public class JaksonJSONSave extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonjson-save";
    }

    @Override
    public String description() {
        return "jaksonjson-save";
    }

    @Override
    public void execute() throws Exception {
        final String jaksonJSON = "src/main/resources/Jaxb/JAKSONJson.json";
        final DataServiceInterface dataServiceInterface = (DataServiceInterface) serviceLocator.getService("Data-service");
        Data data = new Data();
        dataServiceInterface.save(data);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        try {
            mapper.writeValue(new File(jaksonJSON), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
