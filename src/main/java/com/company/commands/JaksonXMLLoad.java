package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;


public class JaksonXMLLoad extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonxml-load";
    }

    @Override
    public String description() {
        return "jaksonxml-load";
    }

    @Override
    public void execute() throws Exception {
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        Data data;
        XmlMapper objectMapper = new XmlMapper();
        File workingFolder = new File("src/main/resources/Jaxb");
        File dataFile = new File(workingFolder,"JAKSONXML.xml");
        data = objectMapper.readValue(dataFile, Data.class);
        dataService.load(data);
        System.out.println("SUCCESS");

    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
