package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.Interfaces.UserServiceInterface;
import com.company.entity.Data;
import com.company.entity.Project;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JaksonXMLSave extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonxml-save";
    }

    @Override
    public String description() {
        return "jaksonxml-save";
    }

    @Override
    public void execute() throws Exception {
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        Data data = new Data();
        dataService.save(data);
        XmlMapper objectMapper = new XmlMapper();
        objectMapper.configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        final String xml = objectWriter.writeValueAsString(data);
        final byte[] data2 = xml.getBytes(StandardCharsets.UTF_8);
        File workingFolder = new File("src/main/resources/Jaxb");
        File dataFile = new File(workingFolder,"JAKSONXML.xml");
        Files.write(dataFile.toPath(),data2);
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
