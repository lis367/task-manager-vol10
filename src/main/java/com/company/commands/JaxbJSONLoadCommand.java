package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

public class JaxbJSONLoadCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbjson-load";
    }

    @Override
    public String description() {
        return "jaxbjson-load";
    }

    @Override
    public void execute() throws Exception {
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        try {
            final String jaxbJSON = "src/main/resources/Jaxb/JAXBJson.json";
            System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Unmarshaller um = context.createUnmarshaller();
            um.setProperty(UnmarshallerProperties.MEDIA_TYPE,
                    "application/json");
            um.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
            Data data = (Data) um.unmarshal(new InputStreamReader(
                    new FileInputStream(jaxbJSON)));
            dataService.load(data);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
