package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;
import org.eclipse.persistence.jaxb.MarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class JaxbJSONSaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbjson-save";
    }

    @Override
    public String description() {
        return "jaxbjson-save";
    }

    @Override
    public void execute() throws Exception {
        final String jaxbJSON = "src/main/resources/Jaxb/JAXBJson.json";
        try {
            final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
            final Data data = new Data();
            dataService.save(data);
            System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE,
                    "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(data,new File(jaxbJSON));
            System.out.println("SUCCESS");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
