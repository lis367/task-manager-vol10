package com.company.commands;

import com.company.Interfaces.DataServiceInterface;
import com.company.entity.Data;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class JaxbXMLLoadCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbxml-load";
    }

    @Override
    public String description() {
        return "Load JaXb in XML";
    }

    @Override
    public void execute() throws Exception {
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        final String repo_XML = "src/main/resources/Jaxb/JAXBXml.xml";
        JAXBContext context = JAXBContext.newInstance(Data.class);
        Unmarshaller um = context.createUnmarshaller();
        Data data = (Data) um.unmarshal(new InputStreamReader(
                new FileInputStream(repo_XML), StandardCharsets.UTF_8));
        dataService.load(data);
        System.out.println("Success");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
