package com.company.commands;

import com.company.Interfaces.*;
import com.company.entity.Data;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class JaxbXMLSaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbxml-save";
    }

    @Override
    public String description() {
        return "Save in JaXb in XML";
    }

    @Override
    public void execute() throws Exception {
        final DataServiceInterface dataService = (DataServiceInterface) serviceLocator.getService("Data-service");
        final Data data = new Data();
        final String jaxbXML = "src/main/resources/Jaxb/JAXBXml.xml";
        try {
            dataService.save(data);
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(data,new File(jaxbXML));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
