package com.company.commands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.Interfaces.UserServiceInterface;
import org.jetbrains.annotations.NotNull;
import lombok.NoArgsConstructor;
@NoArgsConstructor

public class LoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-command";
    }

    @Override
    public String description() {
        return "Load Users, Projects, Task from DB";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface)serviceLocator.getService("User-Service");
        final ProjectServiceInterface projectServiceInterface = (ProjectServiceInterface) serviceLocator.getService("Project-service");
        final TaskServiceInterface taskServiceInterface = (TaskServiceInterface) serviceLocator.getService("Task-service");

        userServiceImpl.load();
        System.out.println("База пользователей успешно загружена");
        projectServiceInterface.load();
        System.out.println("База проектов успешно загружена");
        taskServiceInterface.load();
        System.out.println("База задач успешно загружена");

    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
