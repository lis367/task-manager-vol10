package com.company.commands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.Interfaces.UserServiceInterface;


public class SaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "save-command";
    }

    @Override
    public String description() {
        return "Save Users, Projects, Tasks";
    }

    @Override
    public void execute() throws Exception {
        final UserServiceInterface userServiceImpl = (UserServiceInterface)serviceLocator.getService("User-Service");
        final ProjectServiceInterface projectServiceInterface = (ProjectServiceInterface) serviceLocator.getService("Project-service");
        final TaskServiceInterface taskServiceInterface = (TaskServiceInterface) serviceLocator.getService("Task-service");


        userServiceImpl.saveInDb();
        projectServiceInterface.saveInDb();
        taskServiceInterface.saveInDb();
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return true;
    }
}
