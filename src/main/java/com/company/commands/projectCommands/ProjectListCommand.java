package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.exception.EmptyField;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.*;

@NoArgsConstructor

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() throws EmptyField {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");
        System.out.println("[PROJECT LIST]");
        ArrayList <Project> projectPool = projectService.projectList();
        System.out.println("How do you want to sort? Press 1 - by creation date, Press 2 - by date begin, " +
                "Press 3 - by date end, Press 4 - by status");
        System.out.println("Press 5 - if you want to search by name or description");
        String line = terminalService.nextLine();
        if(line.isEmpty()){
            throw new EmptyField();
        }
        switch (line){
            case "1":{
                //Вывод в порядке создания
                Collections.sort(projectPool, Comparator.comparing(Project::getCreationDate));
                break;
            }
            case"2":{
                //Вывод по даче начала
                Collections.sort(projectPool, Comparator.comparing(Project::getDateBegin));
                break;
            }
            case"3":{
                //Вывод по дате окончания
                Collections.sort(projectPool, Comparator.comparing(Project::getDateEnd));
                break;
            }
            case "4":{
                //Вывод по статусу
                Collections.sort(projectPool, Comparator.comparing(Project::getDisplayName));
                break;
            }
            case "5":{
                System.out.println("PLEASE ENTER NAME OR DERSCRIPTION");
                line= terminalService.nextLine();
                projectPool = searchByString(line, projectPool);
                break;
            }

        }

        for(int i=0; i<projectPool.size();i++){
            System.out.println("Project Name: "+projectPool.get(i).getName() + " Project ID: " + projectPool.get(i).getId()
                    + " Project description: "+projectPool.get(i).getDescription()
                    + " Project date start: "+simpleDateFormat.format(projectPool.get(i).getDateBegin())
                    + " Project date end: "+simpleDateFormat.format(projectPool.get(i).getDateEnd())
                    + " Project status is: "+projectPool.get(i).getDisplayName());
         }

    }
    public boolean secureCommand() {
        return true;
    }

    public ArrayList searchByString(String name, ArrayList <Project> projectPool){
        ArrayList<Project> arrayWithName = new ArrayList();
        for(Project array: projectPool){
            if(array.getName().contains(name)||array.getDescription().contains(name)){
                arrayWithName.add(array);
            }
        }

        return arrayWithName;
    }


    public ProjectListCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
