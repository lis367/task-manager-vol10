package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.exception.NoPermission;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import java.text.SimpleDateFormat;
import java.util.Scanner;
@NoArgsConstructor

public class ProjectViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");
        try{
            if (projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
                System.out.println("Project name "+ projectService.read(line).getName());
                System.out.println("Project begins in "+ simpleDateFormat.format(projectService.read(line).getDateBegin()));
                System.out.println("Project ends in "+simpleDateFormat.format(projectService.read(line).getDateEnd()));
                System.out.println("Project status is "+(projectService.read(line).getDisplayName()));
            }
            else {
               throw new NoPermission();
            }
        }
        catch (NullPointerException npe){
            throw new ObjectIsNotFound();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectViewCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
