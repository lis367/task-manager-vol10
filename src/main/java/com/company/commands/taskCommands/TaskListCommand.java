package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.EmptyField;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@NoArgsConstructor

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        @NotNull
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-service");
        System.out.println("[TASK LIST]");
        ArrayList<Task> tasksPool = taskServiceImpl.taskList();

        System.out.println("How do you want to sort? Press 1 - by creation date, Press 2 - by date begin, " +
                "Press 3 - by date end, Press 4 - by status");
        System.out.println("Press 5 - if you want to search by name or description");
        String line = terminalService.nextLine();
        if(line.isEmpty()){
            throw new EmptyField();
        }
        switch (line){
            case "1":{
                //Вывод в порядке создания
                Collections.sort(tasksPool, Comparator.comparing(Task::getCreationDate));
                break;
            }
            case"2":{
                //Вывод по даче начала
                Collections.sort(tasksPool, Comparator.comparing(Task::getDateBegin));
                break;
            }
            case"3":{
                //Вывод по дате окончания
                Collections.sort(tasksPool, Comparator.comparing(Task::getDateEnd));
                break;
            }
            case "4":{
                //Вывод по статусу
                Collections.sort(tasksPool, Comparator.comparing(Task::getDisplayName));
                break;
            }
            case "5":{
                {
                    System.out.println("PLEASE ENTER NAME OR DERSCRIPTION");
                    line= terminalService.nextLine();
                    tasksPool= searchByString(line, tasksPool);
                    break;
                }
            }
        }
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");

        for(int i=0;i<tasksPool.size();i++){
            System.out.println("Task Name: "+tasksPool.get(i).getName() + " Task ID: " + tasksPool.get(i).getId()
                    + " Task description is: "+tasksPool.get(i).getDescription()
                    + " Task date start: "+ simpleDateFormat.format(tasksPool.get(i).getDateBegin())
                    + " Task date end: " + simpleDateFormat.format(tasksPool.get(i).getDateEnd())
                    + " Task status is : "+tasksPool.get(i).getDisplayName() + " Project :"+tasksPool.get(i).getProjectID());
        }
    }

    public ArrayList searchByString(String name, ArrayList <Task> projectPool){
        ArrayList<Task> arrayWithName = new ArrayList();
        for(Task array: projectPool){
            if(array.getName().contains(name)||array.getDescription().contains(name)){
                arrayWithName.add(array);
            }
        }

        return arrayWithName;
    }

    public boolean secureCommand() {
        return true;
    }

    public TaskListCommand(ServiceLocator bootstrap) { setServiceLocator(bootstrap);
    }

}
