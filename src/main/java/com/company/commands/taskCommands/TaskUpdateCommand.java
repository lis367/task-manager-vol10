package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.Status;
import com.company.entity.Task;
import com.company.exception.EmptyField;
import com.company.exception.NoPermission;
import com.company.exception.ObjectIsNotFound;
import com.company.exception.WrongDateFormat;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.text.SimpleDateFormat;
@NoArgsConstructor

public final class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");

        System.out.println("ENTER ID");

        String line = terminalService.nextLine();
        try {
            if (taskServiceImpl.read(line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                Task taskPool = taskServiceImpl.read(line);
                System.out.println("ENTER NEW NAME");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                taskPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
                final String dateStart = terminalService.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE TASK");
                final String dateEnd = terminalService.nextLine().trim();
                if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
                    throw new WrongDateFormat();
                }
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                taskPool.setDateBegin(dateFormatter.parse(dateStart));
                taskPool.setDateEnd(dateFormatter.parse(dateEnd));
                System.out.println("ENTER THE STATUS OF THE TASK");
                System.out.println("1 - Pending, 2 - Running, 3 - Finished");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                switch (line){
                    case "1":
                        taskPool.setDisplayName(Status.PENDING);
                        break;
                    case "2":
                        taskPool.setDisplayName(Status.RUNNING);
                        break;
                    case "3":
                        taskPool.setDisplayName(Status.FINISHED);
                        break;
                }
                taskServiceImpl.update(taskPool);
                System.out.println("SUCCESS");
            } else {
                throw new NoPermission();
            }
        }
        catch (NullPointerException e){
            throw new ObjectIsNotFound();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
