package com.company.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor


public final class Task implements Serializable {
    @NotNull
    private String name;
    @NotNull
    private String id;
    @Nullable
    private String description;
    @NotNull
    private Date dateBegin;
    @NotNull
    private Date dateEnd;
    @NotNull
    private String projectID;
    @NotNull
    private String userId;
    @NotNull
    private Status displayName;
    @NotNull
    private Date creationDate;
    private static final long serialVersionUID = 1L;


    public Task(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
    }

}
