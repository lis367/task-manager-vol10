package com.company.exception;

public class EmptyField extends Exception {
    @Override
    public String toString() {
        return "Empty field. The field should be filed";

    }
}
