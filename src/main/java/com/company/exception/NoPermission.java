package com.company.exception;

public class NoPermission extends Exception {
    @Override
    public String toString() {
        return "No Permission exception";
    }
}
