package com.company.exception;

public class ObjectIsNotFound extends Exception {

    @Override
    public String toString() {
        return "Объект не найден";
    }
}
