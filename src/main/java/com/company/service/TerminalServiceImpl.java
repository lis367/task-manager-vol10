package com.company.service;

import com.company.Interfaces.Service;

import java.util.Scanner;

public class TerminalServiceImpl implements Service {

    public String nextLine(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    @Override
    public String getName() {
        return "Terminal-Service";
    }
}
